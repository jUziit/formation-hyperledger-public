/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { Gateway, Wallets } = require('fabric-network');
const path = require('path');
const fs = require('fs');


async function main() {
    try {
        // load the network configuration
        // TODO

        // Create a new file system based wallet for managing identities.
        // TODO

        // Check to see if we've already enrolled the user.
        // TODO

        // Create a new gateway for connecting to our peer node.
        // TODO

        // Get the network (channel) our contract is deployed to.
        // TODO

        // Get the contract from the network.
        // TODO

        // Evaluate the specified transaction.
        // queryCar transaction - requires 1 argument, ex: ('queryCar', 'CAR4')
        // queryAllCars transaction - requires no arguments, ex: ('queryAllCars')
        // TODO

    } catch (error) {
        console.error(`Failed to evaluate transaction: ${error}`);
        process.exit(1);
    }
}

main();
