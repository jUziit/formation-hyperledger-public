# Création d'une application qui interragie avec la chaincode FabCar

## Préparation

Recupéreration du Wallet déjà créé dans le TP Wallet

## Développement

Compléter les programmes query.js et invoke.js

## Résultat attendu

```bash
$ npm install
$ node query.js
Wallet path: ...fabric-samples/fabcar/javascript/wallet
Transaction has been evaluated, result is:
{"colour":"black","make":"Tesla","model":"S","owner":"Adriana"}
$node invoke.js
Wallet path: ...fabric-samples/fabcar/javascript/wallet
2018-12-11T14:11:40.935Z - info: [TransactionEventHandler]: _strategySuccess: strategy success for transaction "9076cd4279a71ecf99665aed0ed3590a25bba040fa6b4dd6d010f42bb26ff5d1"
Transaction has been submitted
```

