# Installation de l'infrastructure

## Objectif

Dans le cadre de la formation, il est demandé d'installer une seul VM pour l'ensemble de la blockchain.
Le choix est porté sur la blockchain Hyperledger Fabric.

## Guide d'installation

### Prérequis

- VM avec 4 vCpu, 8 Go RAM et 50Go SSD environ
- OS Ubuntu 18.04.4 LTS ou plus
- Git
```bash
sudo apt update
sudo apt install git
```
- RSA Key : https://docs.gitlab.com/ee/ssh/

### Installation

Suivre le support "Mise en place de l'environnement technique".

Récupération du projet sur la VM:
```bash
cd
git clone https://gitlab.com/jUziit/formation-hyperledger-public.git
```

### Validation de l'installation

Démarrage du réseau de test
```bash
cd fabric-samples/test-network
./network.sh down
./network.sh up createChannel
./network.sh deployCC
export CORE_PEER_TLS_ENABLED=true
export CORE_PEER_LOCALMSPID="Org1MSP"
export CORE_PEER_TLS_ROOTCERT_FILE=${PWD}/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt
export CORE_PEER_MSPCONFIGPATH=${PWD}/organizations/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp
export CORE_PEER_ADDRESS=localhost:7051
```

Récupération des valeurs dans la blockchain
```bash
peer chaincode query -C mychannel -n fabcar -c '{"Args":["queryAllCars"]}'
```
Attendu : 
```[{"Key":"CAR0", "Record":{"make":"Toyota","model":"Prius","colour":"blue","owner":"Tomoko"}},{"Key":"CAR1", "Record":{"make":"Ford","model":"Mustang","colour":"red","owner":"Brad"}},{"Key":"CAR2", "Record":{"make":"Hyundai","model":"Tucson","colour":"green","owner":"Jin Soo"}},{"Key":"CAR3", "Record":{"make":"Volkswagen","model":"Passat","colour":"yellow","owner":"Max"}},{"Key":"CAR4", "Record":{"make":"Tesla","model":"S","colour":"black","owner":"Adriana"}},{"Key":"CAR5", "Record":{"make":"Peugeot","model":"205","colour":"purple","owner":"Michel"}},{"Key":"CAR6", "Record":{"make":"Chery","model":"S22L","colour":"white","owner":"Aarav"}},{"Key":"CAR7", "Record":{"make":"Fiat","model":"Punto","colour":"violet","owner":"Pari"}},{"Key":"CAR8", "Record":{"make":"Tata","model":"Nano","colour":"indigo","owner":"Valeria"}},{"Key":"CAR9", "Record":{"make":"Holden","model":"Barina","colour":"brown","owner":"Shotaro"}}]```

Appel d'un smartcontract
```bash
peer chaincode invoke -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls true --cafile ${PWD}/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem -C mychannel -n fabcar --peerAddresses localhost:7051 --tlsRootCertFiles ${PWD}/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt --peerAddresses localhost:9051 --tlsRootCertFiles ${PWD}/organizations/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt -c '{"function":"changeCarOwner","Args":["CAR9","Dave"]}'
```
Attendu : ```2019-12-04 17:38:21.048 EST [chaincodeCmd] chaincodeInvokeOrQuery -> INFO 001 Chaincode invoke successful. result: status:200```

Arrêt du réseau de test :
```bash
./network.sh down
```

### Source documentaire

https://hyperledger-fabric.readthedocs.io/en/latest/prereqs.html<br>
https://hyperledger-fabric.readthedocs.io/en/latest/install.html<br>
https://hyperledger-fabric.readthedocs.io/en/latest/test_network.html<br>


